package perfectsquarecount;

import java.util.*;


public class PerfectSquareCount {
static long N; // for number of test cases
static long l,w; // length and width for each test cases
static Scanner scan = new Scanner(System.in); 


public void npSquares(long ntc){
    System.out.print("Enter length of the grid : ");
    l = scan.nextLong();
    System.out.print("Enter width of the grid : ");
    w = scan.nextLong();
    long result = 0;
    long sum = 0;
    for(int i =1; i<=(l*w); i++){
        result+= i * i;  
        sum+=1;
    }
    
    System.out.println("The total number of perfect squares in grid "+ (l) + "X" + (w) + " is " + sum);
}
    public static void main(String[] args) {
        
        System.out.print("Welcome!!! Enter the number of test cases:  ");
        N = scan.nextLong();
        PerfectSquareCount psc = new PerfectSquareCount();
        for (int i =0; i<N; i++){
            System.out.println("for test case "+ (i+1));
            psc.npSquares(N);
        }
        
    }
    
}
